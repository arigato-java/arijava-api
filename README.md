# ありがとう、ジャバのAPIサーバ

Cloud Run (Knative) で動かす「ありがとう、ジャバ」のAPIサーバです。

今回はRustで作ってます。

## 環境変数

* `AJAPI_ALLOW_ORIGIN`
  * CORS で許可するOrigin
* `AJAPI_SECRETS_JSON`
  * ひみつ情報を書いておくJSONをこの環境変数に直書きしてください。
  * `{"turnstile_secret":"abcd","discord_webhook_url":"efg"}`
  * Google Secret Manager がシークレット単位で料金が発生するのでこんな感じでやってます
* `AJAPI_PORT`
  * listenするtcpポートの番号。指定しなければ3000
