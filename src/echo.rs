use axum::{
    extract::ConnectInfo,
    extract::Request,
    response::{Html, IntoResponse},
};
use html_escape::encode_text_to_string;
use std::net::SocketAddr;

pub async fn echo(
    ConnectInfo(addr): ConnectInfo<SocketAddr>,
    request: Request,
) -> impl IntoResponse {
    let headers = request.headers();
    let mut res = String::with_capacity(4096);
    res += "<!DOCTYPE html><html><head></head><body><pre>\n";
    encode_text_to_string(addr.to_string(), &mut res);
    res += "\n\n";
    for (key, value) in headers.iter() {
        encode_text_to_string(key.as_str(), &mut res);
        res += ": ";
        let val_text = value.to_str().unwrap_or("!! INVALID !!");
        encode_text_to_string(val_text, &mut res);
        res += "\n";
    }
    res += "</pre></body></html>\n";

    Html(res)
}
