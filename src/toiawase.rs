use axum::{
    body::Body,
    debug_handler,
    extract::State,
    http::{HeaderMap, StatusCode},
    response::{IntoResponse, Json, Response},
};
use reqwest_middleware::ClientWithMiddleware;
use serde::{Deserialize, Serialize};
use std::sync::Arc;

use crate::AppState;

#[derive(Serialize)]
struct TurnstileRequest {
    /// Turnstile secret API key
    secret: String,
    /// Turnstile token obtained by client
    response: String,
    /// optional remote ip
    remoteip: Option<String>,
    /// idempotency_key
    idempotency_key: String,
}

#[derive(Deserialize)]
struct TurnstileResponse {
    success: bool,
    #[serde(rename = "error-codes")]
    error_codes: Vec<String>,
}

/// Captchaの検証。true/false、またはネットワークエラーなど正常に検証できなかったパターン
async fn turnstile_verify(
    http_client: &ClientWithMiddleware,
    turnstile_secret: &Arc<str>,
    captcha_token: &str,
    remote_ip: Option<String>,
) -> anyhow::Result<bool> {
    let idempotency_key: u64 = rand::random();

    let res = http_client
        .post("https://challenges.cloudflare.com/turnstile/v0/siteverify")
        .json(&TurnstileRequest {
            secret: String::from(turnstile_secret.as_ref()),
            response: String::from(captcha_token),
            remoteip: remote_ip,
            idempotency_key: format!("{}", idempotency_key),
        })
        .send()
        .await?
        .json::<TurnstileResponse>()
        .await?;
    if !res.success {
        tracing::warn!("Turnstile rejected: {:?}", res.error_codes);
    }
    Ok(res.success)
}

#[derive(Serialize)]
struct DiscordWebhookRequest {
    embeds: Vec<DiscordWebhookEmbed>,
}

#[derive(Serialize)]
struct DiscordWebhookEmbed {
    #[serde(rename = "type")]
    post_type: String,
    description: String,
    color: u32,
}

/// Discord Webhook形式の投稿
async fn post_discord(
    http_client: &ClientWithMiddleware,
    webhook_url: &Arc<str>,
    message: &str,
) -> anyhow::Result<bool> {
    let res = http_client
        .post(webhook_url.as_ref())
        .json(&DiscordWebhookRequest {
            embeds: vec![DiscordWebhookEmbed {
                post_type: "rich".into(),
                description: String::from(message),
                color: 0xee0e60u32,
            }],
        })
        .send()
        .await?
        .status()
        .is_success();

    Ok(res)
}

#[derive(Deserialize)]
pub struct ToiawaseRequest {
    /// captcha token
    captcha: String,
    /// message body
    message: String,
}

#[derive(Serialize)]
struct ToiawaseResponse {
    success: bool,
}

#[debug_handler]
pub async fn toiawase(
    State(state): State<AppState>,
    headers: HeaderMap,
    Json(payload): Json<ToiawaseRequest>,
) -> Response {
    let forwarded_for = headers
        .get("x-forwarded-for")
        .map(|name| String::from(name.to_str().unwrap()));
    let _ = match turnstile_verify(
        &state.reqwest_client,
        &state.turnstile_secret,
        payload.captcha.as_ref(),
        forwarded_for,
    )
    .await
    {
        Ok(true) => true,
        Ok(false) => return (StatusCode::BAD_REQUEST, Body::empty()).into_response(),
        Err(_err) => return (StatusCode::SERVICE_UNAVAILABLE, Body::empty()).into_response(),
    };

    match post_discord(
        &state.reqwest_client,
        &state.discord_webhook_url,
        &payload.message,
    )
    .await
    {
        Ok(true) => (StatusCode::OK, Json(ToiawaseResponse { success: true })).into_response(),
        Ok(false) => (StatusCode::SERVICE_UNAVAILABLE, Body::empty()).into_response(),
        Err(_err) => (StatusCode::SERVICE_UNAVAILABLE, Body::empty()).into_response(),
    }
}
