use axum::{
    extract::DefaultBodyLimit,
    http::header::CACHE_CONTROL,
    http::header::CONTENT_TYPE,
    http::{HeaderValue, Method},
    routing::{get, post},
    Router,
};
use reqwest_middleware::{ClientBuilder, ClientWithMiddleware};
use reqwest_retry::{policies::ExponentialBackoff, RetryTransientMiddleware};
use serde::Deserialize;
use std::net::{Ipv6Addr, SocketAddr, SocketAddrV6};
use std::sync::Arc;
use std::time::Duration;
use tokio::{select, signal};
use tower_http::{cors::CorsLayer, set_header::SetResponseHeaderLayer, trace::TraceLayer};
use tracing_subscriber::{layer::SubscriberExt, util::SubscriberInitExt};

mod echo;
mod toiawase;

const USER_AGENT_STR: &str = concat!(env!("CARGO_PKG_NAME"), "/", env!("CARGO_PKG_VERSION"));

#[derive(Clone)]
struct AppState {
    /// reuse reqwest client
    reqwest_client: ClientWithMiddleware,
    /// Turnstile API key
    turnstile_secret: Arc<str>,
    /// Discord Webhook URL
    discord_webhook_url: Arc<str>,
}

#[derive(Deserialize)]
struct AppSecrets {
    /// Turnstile API key
    turnstile_secret: String,
    /// Discord Webhook URL
    discord_webhook_url: String,
}

fn init_app_state() -> anyhow::Result<AppState> {
    let secrets_env =
        std::env::var("AJAPI_SECRETS_JSON").expect("env var AJAPI_SECRETS_JSON not found");
    let secrets: AppSecrets =
        serde_json::from_str(&secrets_env).expect("invalid json in AJAPI_SECRETS_JSON");
    let retry_policy = ExponentialBackoff::builder().build_with_max_retries(2);
    Ok(AppState {
        reqwest_client: ClientBuilder::new(
            reqwest::Client::builder()
                .user_agent(USER_AGENT_STR)
                .build()
                .unwrap(),
        )
        .with(RetryTransientMiddleware::new_with_policy(retry_policy))
        .build(),
        turnstile_secret: secrets.turnstile_secret.into(),
        discord_webhook_url: secrets.discord_webhook_url.into(),
    })
}

#[tokio::main]
async fn main() {
    tracing_subscriber::registry()
        .with(
            tracing_subscriber::EnvFilter::try_from_default_env()
                .unwrap_or_else(|_| "arijava_api=info,tower_http=info".into()),
        )
        .with(tracing_subscriber::fmt::layer().json())
        .init();

    let state = init_app_state().unwrap();
    let allow_origin =
        std::env::var("AJAPI_ALLOW_ORIGIN").expect("env var AJAPI_ALLOW_ORIGIN not found");
    let port = match std::env::var("AJAPI_PORT") {
        Ok(x) => x.parse::<u16>().expect("invalid integer in AJAPI_PORT"),
        Err(_) => 3000u16,
    };
    let app = Router::new()
        .route("/toiawase", post(toiawase::toiawase))
        .route_layer(
            CorsLayer::new()
                .allow_origin(allow_origin.parse::<HeaderValue>().unwrap())
                .allow_headers([CONTENT_TYPE])
                .allow_methods([Method::POST])
                .max_age(Duration::from_secs(86400)),
        )
        .route("/echo", get(echo::echo))
        .with_state(state)
        .layer(DefaultBodyLimit::max(640 * 1024)) // 640 KB ought to be enough for anyone
        .layer(SetResponseHeaderLayer::if_not_present(
            CACHE_CONTROL,
            HeaderValue::from_static("private"),
        ))
        .layer(TraceLayer::new_for_http());

    let addr: SocketAddr =
        std::net::SocketAddr::V6(SocketAddrV6::new(Ipv6Addr::UNSPECIFIED, port, 0, 0));
    let listener = tokio::net::TcpListener::bind(addr).await.unwrap();
    axum::serve(
        listener,
        app.into_make_service_with_connect_info::<SocketAddr>(),
    )
    .with_graceful_shutdown(shutdown_handler())
    .await
    .unwrap();
}

async fn shutdown_handler() {
    let ctrl_c = async {
        signal::ctrl_c()
            .await
            .expect("failed to install Ctrl+C handler");
    };

    let terminate = async {
        signal::unix::signal(signal::unix::SignalKind::terminate())
            .unwrap()
            .recv()
            .await;
    };

    select! {
        _ = ctrl_c => {},
        _ = terminate => {},
    }
}
