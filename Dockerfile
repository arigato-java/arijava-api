FROM rust:1.85.0-alpine3.21 as builder
RUN apk add --no-cache libc-dev
WORKDIR /app

COPY Cargo.toml Cargo.lock ./
COPY src src

RUN cargo build --release

FROM scratch
COPY --from=builder /app/target/release/arijava-api /arijava-api
ENTRYPOINT ["/arijava-api"]
